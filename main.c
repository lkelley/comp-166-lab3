#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double density(double height);

const int iterations = 100000;

const double g = 9.81;

double height;
double mass;
double dragCoefficient;
double xSecArea;
double timeStep;
double velocity;
double time;

double calculateAcceleration() {
    double gravity = mass * g;
    double drag = -0.5 * dragCoefficient * density(height) * xSecArea * pow(velocity, 2);

    return (gravity + drag) / mass;
}

int main() {
    // Prompt for height, store
    printf("Enter the height in metres: ");
    scanf("%lf", &height);

    // Prompt for mass
    printf("Enter the mass in kg: ");
    scanf("%lf", &mass);

    // Prompt for drag coefficient
    printf("Enter the drag coefficient: ");
    scanf("%lf", &dragCoefficient);

    // Prompt for cross-sectional area
    printf("Enter the cross-sectional area in m^2: ");
    scanf("%lf", &xSecArea);

    // Prompt for the time step
    printf("Enter the time step in seconds: ");
    scanf("%lf", &timeStep);

    velocity = 0; // Set initial velocity
    time = 0; // Set initial time

    for (int i = 1; i <= iterations; i++) {
        // Print status
        printf("%.2f\t%.1f\t%.1f\n", time, height, velocity);

        // Determine acceleration at the end of the last step
        double acceleration = calculateAcceleration();

        // Increment the height
        height -= velocity * timeStep - 0.5 * acceleration * pow(timeStep, 2);

        // Increment the velocity using the acceleration from the beginning of this step
        velocity += acceleration * timeStep;

        if (height < 0) break;

        // Increment time
        time += timeStep;
    }

    return EXIT_SUCCESS;
}
