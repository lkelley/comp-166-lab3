/*
 * Compute the density of dry air in SI units.
 *
 * @param altitude Altitude in metres
 *
 * @return Density in SI units
 */

#include <math.h>

double density(double height) {
    const double p0 = 101.325e3; // Sea-level std atmospheric pressure, Pascals
    const double T0 = 288.15;    // Sea-level std temperature, Kelvins
    const double L = 0.0065;    // Temperature lapse rate, K/m
    const double R = 8.31447;   // Ideal gas constant, J/(mol.K)
    const double M = 0.0289644; // Molar mass of dry air, kg/mol
    const double g = 9.81; // Gravitational acceleration in m/s^2

    double p, T;
    T = T0 - L * height;
    p = p0 * pow((1 - (L * height) / T0), (g * M) / (R * L));
    return p * M / (R * T);
}
